# **********************************************************************
# * Copyright (C) 2017 Authors
# *
# * Authors: Adrian, Dolphin Oracle
# *          MX Linux http://mxlinux.org
# *          Garuda Linux <https://garudalinux.org/>
# *          Generic Apps Project <https://gitlab.com/liya5/generic-apps/>
# * This is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this package. If not, see <http://www.gnu.org/licenses/>.
# **********************************************************************/

QT       += core gui
CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = boot-manager
TEMPLATE = app

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

# Disable QDebug on Release build
#CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Unused not working
#CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR

       help.path=$$PREFIX/share/doc/boot-manager/help/
       help.files=help/boot-manager.html

        icons.path = $$PREFIX/share/pixmaps/
        icons.files = boot-manager.png

        desktop.path = $$PREFIX/share/applications/
        desktop.files = "boot-manager.desktop"

        INSTALLS += target  desktop help icons
}

SOURCES += main.cpp\
    mainwindow.cpp \
    dialog.cpp \
    mprocess.cpp

HEADERS  += \
    mainwindow.h \
    dialog.h \
    version.h \
    mprocess.h

FORMS    += \
    mainwindow.ui

RESOURCES += \
    images.qrc

